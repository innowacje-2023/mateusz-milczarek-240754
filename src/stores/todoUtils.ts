import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import { db } from '@/main'
import {
  collection,
  getDocs,
  query,
  where,
  addDoc,
  doc,
  updateDoc,
  deleteDoc
} from 'firebase/firestore'
import { getAuth } from 'firebase/auth'

export interface Task {
  name: string
  done: boolean
  readyToEditName: boolean
  id: number
  docId: string
}

export const useTodoStore = defineStore('utils', () => {
  let id = 0
  let userID = ''
  const currentUser = getAuth().currentUser

  currentUser ? (userID = currentUser.uid) : console.log('No user currently logged in')

  const tasksCollectionRef = collection(db, 'tasks')
  const getTasksQuery = getDocs(query(tasksCollectionRef, where('uid', '==', userID)))

  const name = ref('')
  const hideDoneTasks = ref(false)

  const todoList = ref<Task[]>([])

  const onSuccess = (docs) => {
    if (Array.isArray(docs)) {
      todoList.value = docs.map((item) => item.data())
    } else {
      console.error('Invalid data format:', docs)
    }
  }

  getTasksQuery
    .then((snapshot) => {
      const docs = snapshot.docs
      onSuccess(docs)
    })
    .catch((error) => {
      console.error('Error retrieving documents:', error)
    })

  async function addTask() {
    const newTask: Task = {
      id: id++,
      name: name.value.toUpperCase(),
      done: false,
      readyToEditName: false,
      docId: ''
    }
    const data = {
      name: name.value.toUpperCase(),
      done: false,
      uid: userID,
      docId: ''
    }
    const docRef = await addDoc(tasksCollectionRef, data)
    newTask.docId = docRef.id
    const taskRef = doc(db, 'tasks', newTask.docId)
    await updateDoc(taskRef, {
      docId: docRef.id
    })
    todoList.value.push(newTask)
    name.value = ''
  }

  async function removeTask(task) {
    await deleteDoc(doc(db, 'tasks', task.docId))
    todoList.value = todoList.value.filter((t) => t !== task)
  }

  async function updateDoneStatus(task) {
    task.done = !task.done
    const taskRef = doc(db, 'tasks', task.docId)
    await updateDoc(taskRef, {
      done: task.done
    })
  }

  function setTaskToEdit(task) {
    task.readyToEditName = !task.readyToEditName
  }

  async function editTaskName(newName, task) {
    task.name = newName.toUpperCase()
    task.readyToEditName = false
    const taskRef = doc(db, 'tasks', task.docId)
    await updateDoc(taskRef, {
      name: newName.toUpperCase()
    })
  }

  function sortTodoList() {
    todoList.value = todoList.value.sort((a, b) => a.name.localeCompare(b.name))
  }

  const filteredTodoList = computed(() => {
    return hideDoneTasks.value ? todoList.value.filter((t) => !t.done) : todoList.value
  })

  const unexecutedTasksCounter = computed(() => {
    return todoList.value.reduce((accumulator, current) => {
      return accumulator + (current.done ? 0 : 1)
    }, 0)
  })

  const rules = ref([
    (value) => {
      if (value.length > 3 || value === null) return true
      return 'Minimum 4 znaki'
    }
  ])

  return {
    addTask,
    sortTodoList,
    name,
    hideDoneTasks,
    todoList,
    filteredTodoList,
    unexecutedTasksCounter,
    updateDoneStatus,
    editTaskName,
    setTaskToEdit,
    removeTask,
    rules
  }
})
