import { ref } from 'vue'
import { defineStore } from 'pinia'
import { useRouter } from 'vue-router'
import {
  getAuth,
  createUserWithEmailAndPassword,
  signInWithEmailAndPassword,
  GoogleAuthProvider,
  signInWithPopup,
  UserCredential
} from 'firebase/auth'

export const useUserStore = defineStore('user', () => {
  const email = ref('')
  const password = ref('')
  const name = ref<string | undefined>('')
  const userAuth = ref<UserCredential | null>(null)
  const router = useRouter()
  const isLoggedIn = ref(false)

  const register = () => {
    createUserWithEmailAndPassword(getAuth(), email.value, password.value)
      .then((userCredentials) => {
        userAuth.value = userCredentials
        name.value = userCredentials?.user.displayName || undefined
        console.log('Successfully registered!')
        router.push('/dashboard')
      })
      .catch((error) => {
        console.log(error.message)
        alert(error.message)
      })
  }

  const registerGoogle = () => {
    const provider = new GoogleAuthProvider()
    signInWithPopup(getAuth(), provider)
      .then((userCredentials) => {
        userAuth.value = userCredentials
        name.value = userCredentials.user?.displayName || undefined
        console.log('Successfully registered with google!')
        router.push('/dashboard')
      })
      .catch((error) => {
        console.log(error.message)
      })
  }

  const signIn = () => {
    signInWithEmailAndPassword(getAuth(), email.value, password.value)
      .then((userCredentials) => {
        userAuth.value = userCredentials
        name.value = userCredentials.user?.displayName || undefined
        console.log('Successfully logged in!')
        router.push('/dashboard')
      })
      .catch((error) => {
        console.log(error.message)
        alert(error.message)
      })
  }

  return {
    email,
    password,
    register,
    registerGoogle,
    signIn,
    name,
    userAuth,
    isLoggedIn
  }
})
