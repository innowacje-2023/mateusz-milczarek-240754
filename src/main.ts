import { createApp, watch } from 'vue'
import { createPinia } from 'pinia'

import App from './App.vue'
import router from './router'

import 'vuetify/styles'
import { createVuetify } from 'vuetify'
import * as components from 'vuetify/components'
import * as directives from 'vuetify/directives'

import { aliases, mdi } from 'vuetify/iconsets/mdi-svg'

import { createI18n } from 'vue-i18n'

import Polski from '@/locales/pl.json'
import English from '@/locales/eng.json'

import { initializeApp } from 'firebase/app'
import { getFirestore } from 'firebase/firestore'
// import { getAnalytics } from 'firebase/analytics'

const vuetify = createVuetify({
  components,
  directives,
  theme: {
    defaultTheme: 'dark'
  },
  icons: {
    defaultSet: 'mdi',
    aliases,
    sets: {
      mdi
    }
  }
})

const firebaseConfig = {
  apiKey: 'AIzaSyALQgVi3-UWdMlaPRsswVg5R-by9hRXz28',
  authDomain: 'innowacja-e6b5b.firebaseapp.com',
  projectId: 'innowacja-e6b5b',
  storageBucket: 'innowacja-e6b5b.appspot.com',
  messagingSenderId: '707816272662',
  appId: '1:707816272662:web:b99c3dd118fd333f951637',
  measurementId: 'G-C90KFQKTX7'
}

initializeApp(firebaseConfig)
// const analytics = getAnalytics(app1)

export const db = getFirestore()

const i18n = createI18n({
  locale: 'Polski',
  messages: {
    Polski,
    English
  }
})

const app = createApp(App)

app.use(createPinia())
app.use(router)
app.use(vuetify)
app.use(i18n)

app.mount('#app')

const storedLanguage = localStorage.getItem('language')
if (storedLanguage && (storedLanguage === 'Polski' || storedLanguage === 'English')) {
  i18n.global.locale = storedLanguage
}

watch(
  () => i18n.global.locale,
  (newLocale) => {
    localStorage.setItem('language', newLocale)
  }
)
